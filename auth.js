const jwt = require('jsonwebtoken');

const secret = 'oceanView';

// Authentication for users logging in

module.exports.createAccessToken = (user) => {

	const data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
};

// Verification

module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization;

	if(typeof token !== 'undefined'){

		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) => {

			if(error) {
				return response.send({auth: 'Failed. Please double-check token.'});
			} else {
				next();
			}
		})
	} else {
		return response.send({auth: 'Failed. Please double-check token.'});
	}
};

// Decryption

module.exports.decode = (token) => {
	
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else {
		return null;
	}
};
