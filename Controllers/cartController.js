const mongoose = require('mongoose');
const Booking = require('../Models/bookingSchema');
const Room = require('../Models/roomSchema');
const auth = require('../auth.js');
const Cart = require('../Models/cartSchema');


module.exports.addItem = (request,response) => {
    const userData = auth.decode(request.headers.authorization)
    let input = request.body
     
    if(userData.isAdmin){
        response.send("Please login as a user!")
    } else {
         Room.findOne({roomId: input.roomId})
         .then(result => {
        let addedItem = new Cart ({
            userId: userData._id,
            cart: [{
                roomId:input.roomId,
                quantity:input.quantity,
                price: result.price
            }],
            subTotal:result.price * input.quantity
        })
          addedItem.save()
          .then(result => {
            const {_id,__v, ...others} = result._doc
            return response.send({...others})
          })
          .catch(error => {
            response.send(false)
          })
        })
    }
}


// insert in addedItem
module.exports.insertRoom = async (request,response) =>{
    const userData = auth.decode(request.headers.authorization)
    let input = request.body
    if(userData.isAdmin){
        response.send("Please login as a user!")
    } else {
       await Cart.findOne({userId: userData._id})
       .then(result => {
           Room.findOne({roomId: input.roomId})
           .then(data => {
            result.cart.push({roomId:input.roomId, quantity:input.quantity, price: data.price})
            result.save()
            let total = result.cart.map(add => add.price * add.quantity);
             let total2 = total.reduce((a,b)=> (a+b));
             console.log(typeof total2)
             let newSubtotal = {
                subTotal: total2
             }
             Cart.findOneAndUpdate({userId: userData._id}, {$set: newSubtotal}, {new:true})
             .then(result => {
                return response.send(result)
             })
             
           })
           .catch(error => {
            response.send(false)
           })
       })

    }
}





/*//Checkout cart!!
module.exports.checkOut = async(request,response) => {
    const userData = auth.decode(request.headers.authorization)
    let input = request.body
    if(userData.isAdmin){
        response.send("Please login as a user!")
    } else {
      await Cart.findOne({userId: userData._id})
      .then(result => {
          if(result === null){
            response.send("The user id is not existing, cart empty!")
          } else {
            let val = result.room[0]
             console.log(val)
             
          }
      })
      .catch(error =>{
        console.log(error)
        response.send(false)
      })
    }
}*/



//updateCart(subTotal)

module.exports.updateCart = (request,response) => {
   const userData = auth.decode(request.headers.authorization)
   let input = request.body
   if(userData.isAdmin){
      response.send("PLEASE LOGIN AS A USERS")
   } else{
       Cart.findOne({userId:userData._id})
       .then(result => {
         for(let i = 0; i < result.cart.length; i++)
         console.log(result.cart[i])
       })
       .catch(error =>{
        console.log(error)
        return response.send(false)
       })
   }
}

 //deleting cart

 function deleteCart(userIds) {
       Cart.findOneAndDelete({userId: userIds})
       .then(result => {
         return true
       })
       .catch(error => {
        console.log(error)
       })
 }





