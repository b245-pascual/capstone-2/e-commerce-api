
const mongoose = require('mongoose');
const Booking = require('../Models/bookingSchema');
const Users = require('../Models/usersSchema');
const Room = require('../Models/roomSchema');
const auth = require('../auth.js')



//Creating Booking 

module.exports.createBooking = async (req,res) => {
        let input = req.body;
        let roomId = req.body.room
    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
        return res.send("Please login as a user!")
    } else {
         let prodIdCorrect = await Room.findById(roomId)
         .then(result => {
           let bookings = new Booking({
            userId: userData._id,
            bookings: [{roomId: result._id, quantity: input.quantity}],
            subTotal: result.price * input.quantity
           })

           bookings.save()
           .then(save => {
            return res.send(save)
           })
           .catch(error => {
            return res.send(false)
           })
         })
         .catch(error => {
            return res.send("please register first")
         })
    }
}




//GET all booking ADMIN only    (done)

module.exports.getAllBooking = (req,res) => {
    const userData = auth.decode(req.headers.authorization)
    if(!userData.isAdmin){
        res.send("You are not allowed to do that!")
    } else {
        Booking.find()
        .then(result => {
            return res.send(result)
        })
        .catch(error =>{
            return res.send(false)
        })
    }
}

//GET authUser Booking NON-ADMIN only

module.exports.getUserBooking = (req,res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){
        res.send("PLEASE LOGIN AS A USERS!")
    } else {
        Booking.findOne({userId: userData._id})
        .then(result => {
            return res.send(result)
        })
        .catch(error =>{
           return res.send(error)
        })
    }
}





