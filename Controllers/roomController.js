const mongoose = require('mongoose');
const Room = require('../Models/roomSchema.js');
const auth = require('../auth.js');

// Product Creation
module.exports.roomCreate = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const input = request.body;

	if(!userData.isAdmin){
		return response.send('Sorry, you are not authorized to do this. Please contact your administrator if you think this is a mistake.');
	} else {

		Room.findOne({name: input.name})
		.then(result => {

			if(result !== null){
				return response.send('This product already exists. Did you mean to update your product?');
			} else {

				let newRoom = new Room({
					name: input.name,
					description: input.description,
					price: input.price,
					class: input.class
				})

				newRoom.save()
				.then(result => {
					return response.send(`Your rooms ${newRoom.name} has been created!`)
				})
				.catch(error => {
					console.log(error)
					return response.send(`Sorry, there was an error. Please try again.`)
				})
			}
		})
		.catch(error => response.send(err))
	}
};




// View (available rooms)
module.exports.allRooms = (request, response) => {
	Room.find({isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		console.log(err);
		return response.send('Sorry, there was an error. Please try again!')
	})
};

// Queen-size
module.exports.queenSize = (request, response) => {
	Room.find({class: "queenSize", isAvailable: true})
	.then(result => response.send(result))
	.catch(error => {
		console.log(error);
		return response.send('Sorry, there was an error. Please try again!')
	})
};

// (Bunk-beds)
module.exports.bunkBeds = (request, response) => {
	Room.find({class: "bunk beds", isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		console.log(err);
		return response.send('Sorry, there was an error. Please try again!')
	})
};

// singleBed
module.exports.singleBed = (request, response) => {
	Room.find({class: "Single Bed", isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		console.log(err);
		return response.send('Sorry, there was an error. Please try again!')
	})
};


//=============================//



// Retrieve a single product
module.exports.allRoomsOne = (request, response) => {

	const allRoomsId = request.params.allRoomsId;

	Room.findById(allRoomsId)
	.then(result => response.send(result))
	.catch(error => response.send(error))

};

// Edit a specified product
module.exports.roomEdit = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const allRoomsId = request.params.allRoomsId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send('You are not authorized to do this.')
	} else {

		Room.findById({_id: allRoomsId})
		.then(result => {

			if(result === null){
				return response.send('Could not find the product. Please double-check the ID and try again!')
			} else {

				let allRoomsUpdate = {
					name: input.name,
					description: input.description,
					price: input.price,
					class: input.class
				};

				Room.findByIdAndUpdate(allRoomsId, allRoomsUpdate, {new: true})
				.then(result => response.send(result))
				.catch(error => {
					console.log(error);
					return response.send('Sorry, there was an error. Please try again.')
				})
			}
		})
		.catch(error => {
			console.log(error);
			return response.send('Sorry, there was an error. Please try again.')
		});
	}
};

// Archive a specified room
module.exports.roomArchive = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const allRoomsId = request.params.allRoomsId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send('You are not authorized to do this.');
	} else {

		Room.findById({_id: allRoomsId})
		.then(result => {

			if(result === null){
				return response.send('Product not found. Please try again.');
			} else {

				let updatedStatus = {
					isAvailable: input.isAvailable
				};

				Room.findByIdAndUpdate(allRoomsId, updatedStatus, {new: true})
				.then(result => response.send(result))
				.catch(error => {
					console.log(error);
					return response.send('There was an error. Please try again.')
				});
			}
		})
		.catch(error => {
			console.log(error);
			return response.send('There was an error. Please try again.')
		});
	}
};

