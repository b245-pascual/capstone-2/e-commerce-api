

	const mongoose = require('mongoose');
	const User = require('../Models/usersSchema.js');
	const bcrypt = require('bcrypt');
	const auth = require('../auth.js');

	// Create or Register a user on our db/database

	module.exports.userRegistration = (request, response) => {

		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {

			if(result !== null){
				return response.send('A user with this email already exists. Please try again.');
			} else {

				let newUser = new User({
					email: input.email,
					password: bcrypt.hashSync(input.password, 10)
				});
				newUser.save()
				.then(save => response.send('You are now registered. Thank you!'))
				.catch(error => response.send(error));
			}
		})
		.catch(error => {response.send(error)})
	};

						//-------//


	// 	User login-Authentication

	module.exports.userAuth = (request, response) => {

		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {

			if(result === null){
				return response.send('This email is not yet registered. Please register before logging in!');
			} else {

				const passwordCorrect = bcrypt.compareSync(input.password, result.password);

				if(passwordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
				} else {
					return response.send('Password is incorrect.');
				}
			}
		})
		.catch(error => response.send(error));
	};

	//  	User Retrieval
	module.exports.myProfile = (request, response) => {

		const userData = auth.decode(request.headers.authorization);

		User.findById(userData._id)
		.then(result => {
			result.password = "";
			return response.send(result)
		})
		.catch(error => {
			console.log(error);
			return response.send(error)
		});
	};


							//-------//

		
	// Granting/removing users admin access
	module.exports.giveAdmin = (request, response) => {

		const userData = auth.decode(request.headers.authorization);
		const candidateId = request.params.userId;
		const input = request.body;

		if(!userdata.isAdmin){
			return response.send('You are not authorized to do this. Please contact your admin!')
		} else {

			User.findById({_id: candidateId})
			.then(result => {

				if(result === null){
					return response.send('User not found. Please try again.')
				} else {

					let promotion = {
						isAdmin: input.isAdmin
					};

					User.findByIdAndUpdate(candidateId, promotion, {new: true})
					.then(result => response.send(result))
					.catch(error => {
						console.log(error);
						return response.send('Sorry, there was an error. Please try again.')
					})
				}
			})
			.catch(error => {
				console.log(error);
				return response.send('Sorry, there was an error. Please try again.')
			});
		}
	};
