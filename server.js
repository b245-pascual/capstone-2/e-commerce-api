// setup server
const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');

// Routes
const userRoutes = require('./Routes/userRoutes.js');
const roomRoutes = require('./Routes/roomRoutes.js');
const bookingRoutes = require('./Routes/bookingRoutes.js');
const cartRoutes = require('./Routes/cartRoutes.js');


const port = 4001;
const app = express();


// Mongo + Error-Handling

	mongoose.set('strictQuery', true);
	mongoose.connect('mongodb+srv://admin:admin@batch245-pascual.10ztl8b.mongodb.net/Capstone-2_OceanView_Resort?retryWrites=true&w=majority', {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


	let db = mongoose.connection;

	db.on('error', console.error.bind(console, 'Connection Error.'));
	db.once('open', () => {console.log('Cloud database connection successful.')});

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


// Routing
app.use('/user', userRoutes);
app.use('/room', roomRoutes);
app.use('/booking', bookingRoutes)
app.use('/cart', cartRoutes);

// app.use('/order', orderRoutes);


app.listen(port, () => console.log(`Server is live at port ${port}.`));
