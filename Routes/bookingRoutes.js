const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const bookingsController = require('../Controllers/bookingsController')
const auth = require('../auth.js')


router.post('/',auth.verify,bookingsController.createBooking)


//Route for retrieving all Orders ADMIN ONLY
router.get('/allorder',auth.verify,bookingsController.getAllBooking)

//Route for retrieving auth users order non-admin only
router.get('/userBooking', auth.verify, bookingsController.getUserBooking)


module.exports = router