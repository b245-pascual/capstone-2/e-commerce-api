const express = require('express');
const router = express.Router();
const roomController = require('../Controllers/roomController.js');
const auth = require('../auth.js');

// Route for viewing all available rooms
router.get("/allRooms", roomController.allRooms);

// Route for viewing room types
router.get('/allrooms/queenSize', roomController.queenSize);
router.get('/allrooms/bunkBeds', roomController.bunkBeds);
router.get('/allrooms/singleBed', roomController.singleBed);

// Route for seeing a specific room
router.get('/:allRoomsId', roomController.allRoomsOne);

// Route for room creation
router.post('/create', auth.verify, roomController.roomCreate);

// Route for room updating
router.put('/edit/:allRoomsId', auth.verify, roomController.roomEdit);

// Route for archiving a room/updating availability
router.put('/archive/:allRoomsId', auth.verify, roomController.roomArchive);




module.exports = router;