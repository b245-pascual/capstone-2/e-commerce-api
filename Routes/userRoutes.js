const express = require('express');
const router = express.Router();
const userController = require('../Controllers/userController.js');
const auth = require('../auth.js');

// Route for User Registration
router.post('/register', userController.userRegistration);

// Route for User Authentication
router.post('/login', userController.userAuth);

// Route for viewing user details
router.get('/myProfile', auth.verify, userController.myProfile);

// Route for granting/removing admin access
router.post('/giveAdmin/:userId', auth.verify, userController.giveAdmin);


module.exports = router;
