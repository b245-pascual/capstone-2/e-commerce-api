const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const cartController = require('../Controllers/cartController');
const auth = require('../auth');

// add item
router.post('/', auth.verify, cartController.addItem);

//insert item
router.post('/insert', auth.verify, cartController.insertRoom);

/*//Checkout items
router.post('/checkout', auth.verify, cartController.checkOut);*/

//Update Cart items
router.patch('/updateCart', auth.verify, cartController.updateCart);

//deleting items
/*router.post('/deleteCart', auth.verify, cartController.deleteCart);
*/

module.exports = router;