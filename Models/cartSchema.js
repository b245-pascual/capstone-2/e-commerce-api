const mongoose = require('mongoose');


const CartSchema = new mongoose.Schema(
    {
        userId: {
            type:String,
            required: true
        },
        cart : [
            {
                roomId: {
                    type:String,
                },
                quantity: {
                    type:Number,
                    min: 1,
                },
                price: {
                    type:Number
                }
            }
        ],
        subTotal: {
            type:Number
        }
       
    })

module.exports = mongoose.model("Cart", CartSchema);

