const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Please name your room.']
	},
	description: {
		type: String,
		required: [true, 'Please describe your room.']
	},
	price: {
		type: Number,
		required: [true, 'Please enter a price.']
	},
	class: {
		type: String,
		required: [true, 'Please specify room type.']
	},

	isAvailable: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Room", roomSchema);
