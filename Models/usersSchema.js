const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "The EMAIL ADDRESS is required!"]
	},
	password: {
		type: String,
		required: [true, "You need to set up a PASSWORD!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model("User", usersSchema);
