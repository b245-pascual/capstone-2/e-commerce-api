const mongoose = require('mongoose');


const BookingSchema = new mongoose.Schema({
    userId: {
        type:String,
        required: true
    },
    Bookings : [
        {
            roomId: {
                type:String,
            },
            quantity: {
                type:Number,
            },
            price : {
                type:Number
            }
        }
    ],
   subTotal: {
        type:Number
    },
    status: {
        type:String,
        default: "Pending"
    }   
},{timestamps: true})

module.exports = mongoose.model("Booking", BookingSchema);

